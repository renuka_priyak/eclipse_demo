package com;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class PersonController {

	@Autowired
	CountryRepository countryrepo;

	/*@RequestMapping(value = "/{id}")
	public Person getDetails(@PathVariable int id) {
		return countryrepo.findById(id);

	}

	@RequestMapping(value = "/{name}")
	public Person getDetails(@PathVariable String name) {
		return countryrepo.findByName(name);

	}*/

	@RequestMapping(value = "/")
	public ModelAndView welcomePerson(@ModelAttribute("person") Person p) {
		// countryrepo.save(p);
		return new ModelAndView("first");
	}

	@RequestMapping(value = "/add")
	public ModelAndView createPerson(@ModelAttribute("person") Person p) {
		// countryrepo.save(p);
		return new ModelAndView("index");
	}

	@RequestMapping(value = "/view")
	public ModelAndView viewPerson(@ModelAttribute("person") Person p) {
		// countryrepo.save(p);
		Iterable<Person> employeelist = countryrepo.findAll();
		return new ModelAndView("success", "listpersons", employeelist);
	}

	@RequestMapping(value = "/save")
	public ModelAndView savePerson(@ModelAttribute("person") Person p) {
		if (p.getId() == 0) {
			countryrepo.save(p);

		} else {
			countryrepo.save(p);

		}
		Iterable<Person> employeelist = countryrepo.findAll();
		return new ModelAndView("success", "listpersons", employeelist);
	}

	@RequestMapping("/edit/{id}")
	public ModelAndView editPerson(@PathVariable("id") int id) {
		//int id = Integer.parseInt(request.getParameter("id"));
		Person pers = countryrepo.findById(id);
		// Iterable<Person> employeelist=countryrepo.findAll();
		return new ModelAndView("index", "person", pers);
	}

/*	@RequestMapping(value = "/delete")
	public ModelAndView deleteEmployee(@ModelAttribute("person") Person p, HttpServletRequest request) {
		
		int id1 = Integer.parseInt(request.getParameter("id"));
		Person pers = countryrepo.findById(id1);
		System.out.println(id1);
		countryrepo.delete(pers);
		Iterable<Person> employeeList = countryrepo.findAll();
		return new ModelAndView("success","listpersons",employeeList);
		
	}*/
	@RequestMapping(value = "/delete/{id}")
	public ModelAndView deleteEmployee(@PathVariable("id") int id) {
		
		 countryrepo.delete(id);
		Iterable<Person> employeeList = countryrepo.findAll();
		return new ModelAndView("success","listpersons",employeeList);
		
	}
	@RequestMapping(value="/list-page1/{page}")
	public ModelAndView displayPagination(@PathVariable int page)
	{
		Iterable<Person> it=countryrepo.findAll(goTopage(page-1));
		List<Person> ls=iteratorToCollection(it);
		return new ModelAndView("success","listpersons",ls);
	}

	private List<Person> iteratorToCollection(Iterable<Person> it) {
		List<Person> ls=new ArrayList<Person>();
		for(Person p:it)
		{
			ls.add(p);
		}
		return ls;
	}

	public PageRequest goTopage(int page) {
		PageRequest pg=new PageRequest(page, 5);
		
		return pg;
	}
}
	/*@RequestMapping(value="/list-page1")
	public ModelAndView displayPagination2ndAttempt()
	{
		int page=4;
		Iterable<Person> it=countryrepo.findAll(goTopage(page-1));
		List<Person> ls=iteratorToCollection(it);
		return new ModelAndView("success","listpersons",ls);
	}
}

/*
 * @RequestMapping(value="/delete/{id}") public ModelAndView
 * deletePerson(HttpServletRequest request) { int id =
 * Integer.parseInt(request.getParameter("id")); countryrepo.delete(id);
 * Iterable<Person> employeelist=countryrepo.findAll(); return new
 * ModelAndView("success","listpersons",employeelist);
 * 
 * }
 * 
 * @RequestMapping(value="/fetches") public Iterable<Person> fetchAll() { return
 * countryrepo.findAll(); } /*@RequestMapping(value="/edit/{id}") public String
 * updateRecord(@PathVariable int id) { Person p=countryrepo.findById(id);
 * if(p.getId()==0) {
 * 
 * p.setName("Priya kumaraswamy"); p.setCountry("aindaia"); countryrepo.save(p);
 * } else { p.setId(id); p.setName("Priya kumaraswamy");
 * p.setCountry("aindaia"); countryrepo.save(p);
 * 
 * } return "Updated";
 * 
 * }
 * 
 * 
 * @RequestMapping(value="/fetch/{id}") public List<Person> fetch(@PathVariable
 * int id) { return countryrepo.findAllLessThanById(id); }
 */
