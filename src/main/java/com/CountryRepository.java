package com;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CountryRepository extends CrudRepository<Person, Integer>,PagingAndSortingRepository<Person, Integer> {

	Person findById(int id);

	Person findByName(String name);
	 //void updateById(int id);

	List<Person> findAllLessThanById(int id);

}
