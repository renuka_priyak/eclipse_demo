<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
.errors
{
color:red;
font-weight:bold;
}
</style>
</head>
<body>
<div align="center">
 <div class="container">
        <div class="jumbotron">
         <h2>List</h2>
 <!--  <p>The .table-striped class adds zebra-stripes to a table:</p>   -->    
<table border="1" style="border-collapse:collapse" class="table table-striped">
  <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>City</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
<!-- <tr><th>ID</th><th>Name</th><th>City</th></tr> -->

<c:forEach var="listperson" items="${listpersons}">
 <tbody>
<tr>
<td>
<c:out value="${listperson.id}"></c:out></td>
<td><c:out value="${listperson.name}"></c:out></td>
<td><c:out value="${listperson.country}"></c:out></td>
<td><a href="/edit/${listperson.id}"><img  src="/images/edited.png" width="20" height="20"></a></td>
<td><a href="/delete/${listperson.id}"><img  src="/images/delte3.png" width="20" height="20"></a></td>
</tr>
</c:forEach>
 </tbody>
</table>
</div>
</div>

<a href="/list-page1/1">1</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="/list-page1/2">2</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="/list-page1/3">3</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="/list-page1/4">4</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="/add">Add</a>
</div>
</body>
</html>